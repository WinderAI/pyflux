import requests
from flask import Flask
from flask import jsonify
from flask import render_template
from influxdb import InfluxDBClient

app = Flask(__name__)

@app.route('/')
def index():
    host = 'influxdb.marathon.l4lb.thisdcos.directory'
    port = 8086
    user = 'root'
    password = 'root'
    dbname = 'cadvisor'

    # See if we're connected
    try:
        requests.get('http://%s:%s/ping' % (host, port))
        ping = True
    except requests.exceptions.ConnectionError:
        ping = False

    # Now try to get some info
    try:
        client = InfluxDBClient(host, port, user, password, dbname)
        dbs = client.get_list_database()
        series = client.query("SHOW SERIES")

    except requests.exceptions.ConnectionError:
        series = []
        dbs = []

    # Just an extra bit of coolness.
    ifconfig = requests.get("http://ifconfig.co/json").json()

    return render_template('index.html', dbs=dbs, series=series, ifconfig=ifconfig, ping=ping)


@app.route('/ping')
def handle_ping():
    return jsonify({'message': 'pong'})

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80)
